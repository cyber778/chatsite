/**
 * Created by user on 20/11/2015.
 */

function createAlert(elem, text, class_name, delay){
    delay = delay || 1500;
    class_name = class_name || "info";
    var alert_html = '<div class="alert alert-'+class_name+' alert-dismissible fade in" role="alert">' +
      '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
      '<span aria-hidden="true">×</span></button>' + text + '</div>'
    elem.prepend(alert_html);
    setTimeout(function(){
        elem.find('.alert').alert('close');
    },delay);
}
