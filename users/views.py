from django.shortcuts import render
from django.http import JsonResponse, HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.views import logout as auth_logout

# Create your views here.

@csrf_exempt
def login(request):
    ctx = {"success":True}
    u, created = User.objects.get_or_create(username=request.POST["username"])
    if created:
        u.set_password(request.POST["password"])
        u.save()
    user = authenticate(username=request.POST["username"], password=request.POST["password"])
    if user is not None:
        if user.is_active:
            auth_login(request, user)
        else:
            return HttpResponseServerError("user not active")
    else:
        return HttpResponseServerError("wrong username or password")
    return JsonResponse(ctx)


def logout(request, **kwargs):
    return auth_logout(request, 'home', **kwargs)