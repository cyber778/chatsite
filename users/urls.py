__author__ = 'user'


from django.conf.urls import patterns, include, url
from django.contrib import admin
from views import *

urlpatterns = patterns('',
    url(r'^login', login),
    url(r'^logout', logout, name="logout"),
)
